pyCGAL is free software: you can redistribute it and/or modify it under
the terms of the `GNU General Public License version 3 <https://www.gnu.org/licenses/gpl.html>`_.

It is a wrapper around a small part of the CGAL library which has its
`own license scheme <https://github.com/CGAL/cgal/blob/master/LICENSE.md>`_.

The CGAL library is developed in the framework of
the `CGAL project <https://www.cgal.org/project.html>`__ and is maintained
and distributed by `Geometry Factory <https://geometryfactory.com/>`_.
