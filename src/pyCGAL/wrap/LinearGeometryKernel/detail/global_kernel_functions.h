#pragma once

namespace pyCGAL::wrap::LinearGeometryKernel::detail {

template <typename Kernel>
struct global_kernel_functions {};

}  // namespace pyCGAL::wrap::LinearGeometryKernel::detail
