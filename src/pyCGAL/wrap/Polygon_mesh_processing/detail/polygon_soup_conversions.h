#pragma once

namespace pyCGAL::wrap::Polygon_mesh_processing::detail {

template <typename PolygonSoup, typename PolygonMesh>
struct polygon_soup_conversions {};

}  // namespace pyCGAL::wrap::Polygon_mesh_processing::detail
