#pragma once

namespace pyCGAL::wrap::Polygon_mesh_processing::detail {

template <typename TriangleMesh>
struct do_intersect {};

}  // namespace pyCGAL::wrap::Polygon_mesh_processing::detail
