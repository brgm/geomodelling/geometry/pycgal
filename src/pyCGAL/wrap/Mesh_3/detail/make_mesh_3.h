#pragma once

namespace pyCGAL::wrap::Mesh_3::detail {

template <typename C3t3, typename... Domains>
struct make_mesh_3 {};

}  // namespace pyCGAL::wrap::Mesh_3::detail
