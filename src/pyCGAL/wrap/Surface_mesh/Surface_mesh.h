#pragma once

#include <CGAL/IO/GOCAD.h>
#include <CGAL/IO/STL.h>
#ifdef CGAL_USE_VTK
#include <CGAL/IO/VTK.h>
#endif
#include <CGAL/Bbox_3.h>
#include <CGAL/Surface_mesh.h>
#include <pyCGAL/typedefs.h>
#include <pyCGAL/wrap/utils/wrap_index.h>
#include <pyCGAL/wrap/utils/wrap_stl_container.h>

// FIXME: this header should be moved elsewhere
#include <pyCGAL/wrap/Polygon_mesh_processing/utils/property_helpers.h>

#include <memory>

#include "utils/Surface_mesh.h"

namespace pyCGAL {

namespace detail {
template <typename Index, typename Range_type>
auto index_vector_from_range(Range_type& range)
    -> std::unique_ptr<std::vector<Index>> {
  auto v = std::make_unique<std::vector<Index>>();
  assert(v);
  v->insert(v->end(), begin(range), end(range));
  return v;
}

// py::make_iterator fails when building list from iterator
// __next__ must return a value (py::object) not a reference to a py::object
template <typename IRange>
struct Index_range_iterator {
  using iterator = typename IRange::iterator;
  using value_type = typename iterator::value_type;
  IRange range;
  iterator state;
  Index_range_iterator(IRange r) : range{r}, state{begin(r)} {}
  auto __iter__() {
    state = begin(range);
    return *this;
  }
  value_type __next__() {
    if (state == end(range)) throw py::stop_iteration{};
    return *(state++);
  }
};

template <typename IRange>
void wrap_index_range(py::module& module, const std::string& name) {
  using iterator = Index_range_iterator<IRange>;

  py::class_<IRange>(module, name.c_str())
      .def(
          "__iter__", [](IRange& self) { return iterator{self}; },
          py::keep_alive<0, 1>())
      .def("__len__",
           [](const IRange& self) {
             return std::distance(begin(self), end(self));
           })
      .def("as_array", [](IRange& self) {
        using idx_t = decltype(begin(self)->idx());
        const py::ssize_t n = std::distance(begin(self), end(self));
        auto a = py::array_t<idx_t, py::array::c_style>{n};
        auto p = a.mutable_data(0);
        for (auto&& i : self) {
          (*p) = i.idx();
          ++p;
        }
        return a;
      });

  py::class_<iterator>(module, (name + std::string{"_iterator"}).c_str())
      .def("__iter__", &iterator::__iter__)
      .def("__next__", &iterator::__next__);
}

}  // namespace detail

template <typename Point>
typename WrapTraits<CGAL::Surface_mesh<Point>>::py_class wrap_class(
    WrapTraits<CGAL::Surface_mesh<Point>>, py::module& module) {
  namespace wutils = wrap::utils;

  using Wrap = WrapTraits<CGAL::Surface_mesh<Point>>;
  using Surface_mesh = typename Wrap::cpp_type;
  using Surface_mesh_index = typename Surface_mesh::size_type;
  using Vertex_index = typename Surface_mesh::Vertex_index;
  using Halfedge_index = typename Surface_mesh::Halfedge_index;
  using Edge_index = typename Surface_mesh::Edge_index;
  using Face_index = typename Surface_mesh::Face_index;
  using Vertex_range = typename Surface_mesh::Vertex_range;
  using Halfedge_range = typename Surface_mesh::Halfedge_range;
  using Edge_range = typename Surface_mesh::Edge_range;
  using Face_range = typename Surface_mesh::Face_range;

  import_dependencies<Surface_mesh>();

  wutils::wrap_index<Vertex_index, Surface_mesh_index>(module, "Vertex_index");
  wutils::wrap_index<Halfedge_index, Surface_mesh_index>(module,
                                                         "Halfedge_index");
  wutils::wrap_index<Edge_index, Surface_mesh_index>(module, "Edge_index");
  wutils::wrap_index<Face_index, Surface_mesh_index>(module, "Face_index");

  // We deactivate buffer interface to prevent mixing indices with constructors
  auto Vertices_class =
      wutils::wrap_stl_vector<Vertex_index, Surface_mesh_index>(
          module, "Vertices", false, false);
  Vertices_class.def(
      py::init(&detail::index_vector_from_range<Vertex_index, Vertex_range>));
  auto Halfedges_class =
      wutils::wrap_stl_vector<Halfedge_index, Surface_mesh_index>(
          module, "Halfedges", false, false);
  Halfedges_class.def(py::init(
      &detail::index_vector_from_range<Halfedge_index, Halfedge_range>));
  auto Edges_class = wutils::wrap_stl_vector<Edge_index, Surface_mesh_index>(
      module, "Edges", false, false);
  Edges_class.def(
      py::init(&detail::index_vector_from_range<Edge_index, Edge_range>));
  Edges_class.def(py::init([](const Surface_mesh& mesh,
                              const std::vector<Halfedge_index>& halfedges) {
    auto v = std::make_unique<std::vector<Edge_index>>();
    assert(v);
    v->reserve(halfedges.size());
    for (auto&& h : halfedges) {
      v->emplace_back(mesh.edge(h));
    }
    return v;
  }));
  auto Faces_class = wutils::wrap_stl_vector<Face_index, Surface_mesh_index>(
      module, "Faces", false, false);
  Faces_class.def(
      py::init(&detail::index_vector_from_range<Face_index, Face_range>));
  Faces_class.def(py::init([](const Surface_mesh& mesh,
                              const std::vector<Halfedge_index>& halfedges,
                              const bool opposite_faces = false) {
                    auto v = std::make_unique<std::vector<Face_index>>();
                    assert(v);
                    v->reserve(halfedges.size());
                    if (opposite_faces) {
                      for (auto&& h : halfedges) {
                        v->emplace_back(mesh.face(mesh.opposite(h)));
                      }
                    } else {
                      for (auto&& h : halfedges) {
                        v->emplace_back(mesh.face(h));
                      }
                    }
                    return v;
                  }),
                  py::arg("mesh").none(false), py::arg("halfedges").none(false),
                  py::arg("opposite_faces") = false);
  // FIXME: Weirdly enough trying to reserve memory in init methods using
  //        Surface_mesh::reserve generates
  //        Memory Error Bad allocation at run time for big meshes
  //        depending on the python version... Is it linked to direct
  //        contiguous allocation on the heap?
  typename Wrap::py_class pyclass =
      py::class_<Surface_mesh>(module, "Surface_mesh");
  pyclass.def(py::init<>());
  pyclass.def(py::init<const Surface_mesh&>());
  pyclass.def(py::init(
      py::overload_cast<py::list, py::list>(wutils::make_mesh<Surface_mesh>)));
  pyclass.def(py::init([](wutils::Coordinates_array<Point>& vertices,
                          wutils::Face_connectivity<Surface_mesh>& faces) {
    return wutils::make_mesh<Surface_mesh>(vertices, py::make_tuple(faces));
  }));
  pyclass.def(
      py::init(py::overload_cast<wutils::Coordinates_array<Point>&, py::list>(
          wutils::make_mesh<Surface_mesh>)));
  pyclass.def(py::init([](const CGAL::Bbox_3& box) {
    auto p = std::make_unique<Surface_mesh>();
    auto A = p->add_vertex(Point(box.xmin(), box.ymin(), box.zmin()));
    auto B = p->add_vertex(Point(box.xmax(), box.ymin(), box.zmin()));
    auto C = p->add_vertex(Point(box.xmax(), box.ymax(), box.zmin()));
    auto D = p->add_vertex(Point(box.xmin(), box.ymax(), box.zmin()));
    auto E = p->add_vertex(Point(box.xmin(), box.ymin(), box.zmax()));
    auto F = p->add_vertex(Point(box.xmax(), box.ymin(), box.zmax()));
    auto G = p->add_vertex(Point(box.xmax(), box.ymax(), box.zmax()));
    auto H = p->add_vertex(Point(box.xmin(), box.ymax(), box.zmax()));
    p->add_face(A, D, C, B);
    p->add_face(E, F, G, H);
    p->add_face(A, B, F, E);
    p->add_face(B, C, G, F);
    p->add_face(C, D, H, G);
    p->add_face(D, A, E, H);
    return p;
  }));

  pyclass.def("number_of_vertices", &Surface_mesh::number_of_vertices);
  pyclass.def("number_of_halfedges", &Surface_mesh::number_of_halfedges);
  pyclass.def("number_of_edges", &Surface_mesh::number_of_edges);
  pyclass.def("number_of_faces", &Surface_mesh::number_of_faces);
  pyclass.def("is_empty", &Surface_mesh::is_empty);
  pyclass.def("join", &Surface_mesh::join);

  detail::wrap_index_range<Vertex_range>(module, "Surface_mesh_Vertex_range");
  detail::wrap_index_range<Halfedge_range>(module,
                                           "Surface_mesh_Halfedge_range");
  detail::wrap_index_range<Edge_range>(module, "Surface_mesh_Edge_range");
  detail::wrap_index_range<Face_range>(module, "Surface_mesh_Face_range");
  pyclass.def("vertices", &Surface_mesh::vertices, py::keep_alive<0, 1>());
  pyclass.def("halfedges", &Surface_mesh::halfedges, py::keep_alive<0, 1>());
  pyclass.def("edges", &Surface_mesh::edges, py::keep_alive<0, 1>());
  pyclass.def("faces", &Surface_mesh::faces, py::keep_alive<0, 1>());

  pyclass.def("point", py::overload_cast<Vertex_index>(&Surface_mesh::point,
                                                       py::const_));
  pyclass.def("points", [](const Surface_mesh& self,
                           const std::vector<Vertex_index>& vertices) {
    auto result = wutils::empty_coordinates_array<Point>(vertices.size());
    auto p = reinterpret_cast<Point*>(result.mutable_data(0, 0));
    for (auto&& v : vertices) {
      (*p) = self.point(v);
      ++p;
    }
    return result;
  });
  pyclass.def("points", [](const Surface_mesh& self, const py::list& vertices) {
    auto result = wutils::empty_coordinates_array<Point>(py::len(vertices));
    auto p = reinterpret_cast<Point*>(result.mutable_data(0, 0));
    for (auto&& v : vertices) {
      (*p) = self.point(v.cast<Vertex_index>());
      ++p;
    }
    return result;
  });
  pyclass.def("__getitem__", py::overload_cast<Vertex_index>(
                                 &Surface_mesh::point, py::const_));
  pyclass.def("__setitem__", [](Surface_mesh& self, Vertex_index v,
                                const Point& P) { self.point(v) = P; });
  pyclass.def("remove_vertex", &Surface_mesh::remove_vertex);
  pyclass.def("remove_edge", &Surface_mesh::remove_edge);
  pyclass.def("remove_face", &Surface_mesh::remove_face);

  pyclass.def("has_garbage", &Surface_mesh::has_garbage);
  pyclass.def("collect_garbage",
              (void(Surface_mesh::*)()) & Surface_mesh::collect_garbage);

  pyclass.def("__iadd__", [](Surface_mesh& self, const Surface_mesh& other) {
    self += other;
    return self;
  });
  pyclass.def("__add__",
              [](const Surface_mesh& self, const Surface_mesh& other) {
                Surface_mesh copy{self};
                copy += other;
                return copy;
              });
  pyclass.def("add_vertex", py::overload_cast<>(&Surface_mesh::add_vertex));
  pyclass.def("add_vertex",
              py::overload_cast<const Point&>(&Surface_mesh::add_vertex));
  pyclass.def("add_edge", py::overload_cast<>(&Surface_mesh::add_edge));
  pyclass.def("add_edge", py::overload_cast<Vertex_index, Vertex_index>(
                              &Surface_mesh::add_edge));
  // we don't use py::overload_cast<> here because Surface_mesh::add_face
  // also has a template overload (for vertices ranges)
  pyclass.def("add_face", static_cast<Face_index (Surface_mesh::*)()>(
                              &Surface_mesh::add_face));
  pyclass.def("add_face", static_cast<Face_index (Surface_mesh::*)(
                              Vertex_index, Vertex_index, Vertex_index)>(
                              &Surface_mesh::add_face));
  pyclass.def("add_face",
              static_cast<Face_index (Surface_mesh::*)(
                  Vertex_index, Vertex_index, Vertex_index, Vertex_index)>(
                  &Surface_mesh::add_face));
  pyclass.def("add_face", [](Surface_mesh& self, py::sequence vertices) {
    std::vector<Vertex_index> tmp;
    tmp.reserve(py::len(vertices));
    for (auto&& v : vertices) {
      tmp.emplace_back(v.cast<Vertex_index>());
    }
    return self.add_face(tmp);
  });
  pyclass.def(
      "extend",
      [](Surface_mesh& self, wutils::Coordinates_array<Point>& vertices,
         py::list& all_faces, const bool reverse_on_failure,
         const bool throw_on_failure) {
        wutils::Extension_data<Surface_mesh> data;
        wutils::extend_mesh<Surface_mesh>(self, data, vertices, all_faces,
                                          reverse_on_failure, throw_on_failure);
      },
      py::arg("vertices"), py::arg("all_faces"),
      py::arg("reverse_on_failure") = false,
      py::arg("throw_on_failure") = true);

  pyclass.def("centroid", &wutils::centroid<Surface_mesh>);
  pyclass.def("centroids", &wutils::centroids<Surface_mesh>);
  pyclass.def("as_arrays", &wutils::as_arrays<Surface_mesh>,
              py::arg("return_index") = false,
              py::arg("throw_on_breaking_order") = false);
  pyclass.def("as_lists", &wutils::as_lists<Surface_mesh>,
              py::arg("return_vertices_properties") = false,
              py::arg("return_faces_properties") = false);
  pyclass.def("edges_as_lists", &wutils::edges_as_lists<Surface_mesh>,
              py::arg("return_vertices_properties") = false,
              py::arg("return_edges_properties") = false);
  pyclass.def("collect_vertices_properties",
              &wutils::collect_properties<Vertex_index, Surface_mesh>);
  pyclass.def("collect_halfedges_properties",
              &wutils::collect_properties<Halfedge_index, Surface_mesh>);
  pyclass.def("collect_edges_properties",
              &wutils::collect_properties<Edge_index, Surface_mesh>);
  pyclass.def("collect_faces_properties",
              &wutils::collect_properties<Face_index, Surface_mesh>);

  pyclass.def("is_removed", py::overload_cast<Vertex_index>(
                                &Surface_mesh::is_removed, py::const_));
  pyclass.def("is_removed", py::overload_cast<Halfedge_index>(
                                &Surface_mesh::is_removed, py::const_));
  pyclass.def("is_removed", py::overload_cast<Edge_index>(
                                &Surface_mesh::is_removed, py::const_));
  pyclass.def("is_removed", py::overload_cast<Face_index>(
                                &Surface_mesh::is_removed, py::const_));

  pyclass.def("is_valid",
              py::overload_cast<bool>(&Surface_mesh::is_valid, py::const_),
              py::arg("verbose") = true);
  pyclass.def("is_valid", py::overload_cast<Vertex_index>(
                              &Surface_mesh::is_valid, py::const_));
  pyclass.def("is_valid", py::overload_cast<Halfedge_index>(
                              &Surface_mesh::is_valid, py::const_));
  pyclass.def("is_valid", py::overload_cast<Edge_index>(&Surface_mesh::is_valid,
                                                        py::const_));
  pyclass.def("is_valid", py::overload_cast<Face_index>(&Surface_mesh::is_valid,
                                                        py::const_));

  pyclass.def("is_border",
              py::overload_cast<Vertex_index, bool>(&Surface_mesh::is_border,
                                                    py::const_),
              py::arg("v"), py::arg("check_all_incident_halfedges") = true);
  pyclass.def("is_border", py::overload_cast<Halfedge_index>(
                               &Surface_mesh::is_border, py::const_));
  pyclass.def("is_border", py::overload_cast<Edge_index>(
                               &Surface_mesh::is_border, py::const_));

  // Low-Level Connectivity
  pyclass.def("target", &Surface_mesh::target);
  pyclass.def("set_target", &Surface_mesh::set_target);
  pyclass.def("face", &Surface_mesh::face);
  pyclass.def("set_face", &Surface_mesh::set_face);
  pyclass.def("next", &Surface_mesh::next);
  pyclass.def("prev", &Surface_mesh::prev);
  pyclass.def("set_next", &Surface_mesh::set_next);
  pyclass.def("halfedge", py::overload_cast<Vertex_index>(
                              &Surface_mesh::halfedge, py::const_));
  pyclass.def("set_halfedge", py::overload_cast<Vertex_index, Halfedge_index>(
                                  &Surface_mesh::set_halfedge));
  pyclass.def("halfedge", py::overload_cast<Face_index>(&Surface_mesh::halfedge,
                                                        py::const_));
  pyclass.def("set_halfedge", py::overload_cast<Face_index, Halfedge_index>(
                                  &Surface_mesh::set_halfedge));
  pyclass.def("opposite", &Surface_mesh::opposite);

  // Low - Level Connectivity Convenience Functions
  pyclass.def("source", &Surface_mesh::source);
  pyclass.def("next_around_target", &Surface_mesh::next_around_target);
  pyclass.def("prev_around_target", &Surface_mesh::prev_around_target);
  pyclass.def("next_around_source", &Surface_mesh::next_around_source);
  pyclass.def("prev_around_source", &Surface_mesh::prev_around_source);
  pyclass.def("vertex", &Surface_mesh::vertex);
  pyclass.def("halfedge", py::overload_cast<Vertex_index, Vertex_index>(
                              &Surface_mesh::halfedge, py::const_));

  pyclass.def("collect_edges", [](Surface_mesh& self,
                                  const std::vector<Vertex_index>& vertices) {
    auto [flag, created] = self.template add_property_map<Vertex_index, bool>();
    assert(created);
    // will overwrite an existing property map
    // if(!created) std::fill(alternative.begin(), alternative.end(), false);
    for (auto&& v : vertices) flag[v] = true;
    std::vector<Edge_index> result;
    for (auto&& e : self.edges()) {
      if (flag[self.vertex(e, 0)] && flag[self.vertex(e, 1)]) {
        result.emplace_back(e);
      }
    }
    self.remove_property_map(flag);
    return result;
  });
  pyclass.def("collect_edges", [](Surface_mesh& self, py::list& vertices) {
    auto [flag, created] = self.template add_property_map<Vertex_index, bool>();
    assert(created);
    // will overwrite an existing property map
    // if(!created) std::fill(alternative.begin(), alternative.end(), false);
    for (auto&& v : vertices) flag[v.cast<Vertex_index>()] = true;
    std::vector<Edge_index> result;
    for (auto&& e : self.edges()) {
      if (flag[self.vertex(e, 0)] && flag[self.vertex(e, 1)]) {
        result.emplace_back(e);
      }
    }
    self.remove_property_map(flag);
    return result;
  });

  // Switching between Halfedges and Edges
  pyclass.def("edge", &Surface_mesh::edge);
  pyclass.def("edge", [](Surface_mesh& self, const Vertex_index v1,
                         const Vertex_index v2) {
    const auto h = self.halfedge(v1, v2);
    if (h == self.null_halfedge())
      throw std::runtime_error("Non connected vertices.");
    return self.edge(h);
  });

  pyclass.def("halfedge", py::overload_cast<Edge_index>(&Surface_mesh::halfedge,
                                                        py::const_));
  pyclass.def("halfedge", py::overload_cast<Edge_index, unsigned int>(
                              &Surface_mesh::halfedge, py::const_));

  // Degree Functions
  pyclass.def("degree", py::overload_cast<Vertex_index>(&Surface_mesh::degree,
                                                        py::const_));
  pyclass.def("degree",
              py::overload_cast<Face_index>(&Surface_mesh::degree, py::const_));

  // Null Elements
  pyclass.def_property_readonly("null_vertex", [](const Surface_mesh&) {
    return Surface_mesh::null_vertex();
  });
  pyclass.def_property_readonly("null_face", [](const Surface_mesh&) {
    return Surface_mesh::null_face();
  });
  pyclass.def_property_readonly("null_halfedge", [](const Surface_mesh&) {
    return Surface_mesh::null_halfedge();
  });
  pyclass.def_property_readonly("null_edge", [](const Surface_mesh&) {
    return Surface_mesh::null_edge();
  });

  pyclass.def("read_off", &wutils::read_off<Point>);
  pyclass.def("write_off", &wutils::write_off<Point>);
  pyclass.def_static("from_off", [](const std::string& filename) {
    auto p = std::make_unique<Surface_mesh>();
    wutils::read_off(*p, filename);
    return p;
  });
  pyclass.def(
      "write_GOCAD",
      [](const Surface_mesh& self, const std::string& filename,
         const bool verbose) {
        CGAL::IO::write_GOCAD(filename, self,
                              CGAL::parameters::verbose(verbose));
      },
      py::arg("filename").none(false), py::arg("verbose") = false);
  pyclass.def(
      "write_STL",
      [](const Surface_mesh& self, const std::string& filename,
         const bool verbose) {
        CGAL::IO::write_STL(filename, self, CGAL::parameters::verbose(verbose));
      },
      py::arg("filename").none(false), py::arg("verbose") = false);
#ifdef CGAL_USE_VTK
  pyclass.def(
      "write_VTP",
      [](const Surface_mesh& self, const std::string& filename,
         const bool use_binary_mode, const int stream_precision) {
        CGAL::IO::write_VTP(filename, self,
                            CGAL::parameters::use_binary_mode(use_binary_mode)
                                .stream_precision(stream_precision));
      },
      py::arg("filename").none(false), py::arg("use_binary_mode") = true,
      py::arg("stream_precision") = 6);
#endif

  auto process_mesh_output =
      [](Surface_mesh* pmesh, wutils::Extension_data<Surface_mesh> data,
         bool with_vertices_map, bool with_faces_map) -> py::object {
    auto mesh = py::cast(pmesh);
    if (with_vertices_map) {
      if (with_faces_map) return py::make_tuple(mesh, data.vmap, data.fmap);
      return py::make_tuple(mesh, data.vmap);
    } else {
      if (with_faces_map) return py::make_tuple(mesh, data.fmap);
    }
    return mesh;
  };

  pyclass.def(
      "insert_isovalue",
      [](Surface_mesh& self, py::function& f,
         py::object edge_is_constrained_map) {
        using Edge_index = typename Surface_mesh::Edge_index;
        using Edge_flag =
            typename Surface_mesh::template Property_map<Edge_index, bool>;
        Edge_flag is_constrained;
        try {
          is_constrained = py::cast<Edge_flag>(edge_is_constrained_map);
        } catch (py::cast_error) {
          std::string name{py::cast<std::string>(edge_is_constrained_map)};
          auto&& [pmap, created] =
              self.template add_property_map<Edge_index, bool>(name, false);
          // assert(created); // there's no real reason to force creation
          is_constrained = pmap;
        }
        wrap::utils::insert_isovalue(
            self, [f](const Point& P) { return py::cast<double>(f(P)); },
            is_constrained, true);
      },
      py::arg("f").none(false), py::arg("edge_is_constrained_map").none(false));

  pyclass.def(
      "insert_isovalue",
      [](Surface_mesh& self, py::function& f, py::object edge_rank_map,
         int rank) {
        using Edge_index = typename Surface_mesh::Edge_index;
        using Edge_map =
            typename Surface_mesh::template Property_map<Edge_index, int>;
        Edge_map rank_map;
        try {
          rank_map = py::cast<Edge_map>(edge_rank_map);
        } catch (py::cast_error) {
          std::string name{py::cast<std::string>(edge_rank_map)};
          auto&& [pmap, created] =
              self.template add_property_map<Edge_index, int>(name, 0);
          assert(created);
          rank_map = pmap;
        }
        wrap::utils::insert_isovalue(
            self, [f](const Point& P) { return py::cast<double>(f(P)); },
            rank_map, rank);
      },
      py::arg("f").none(false), py::arg("edge_rank_map").none(false),
      py::arg("rank"));

  pyclass.def(
      "split_edge_constraints_into_polylines",
      [](Surface_mesh& self, py::object constraints) {
        return wrap::utils::split_edge_constraints_into_polylines(self,
                                                                  constraints);
      },
      py::arg("constraints").none(false));

  pyclass.def(
      "pack_cells_along_edges",
      [](Surface_mesh& self, py::object edge_is_constrained,
         py::str pack_property_name) {
        using Edge_index = typename Surface_mesh::Edge_index;

        auto is_constrained_edge_option =
            wutils::convert_to_property_flag<Edge_index>(edge_is_constrained,
                                                         self, true);
        if (!is_constrained_edge_option) {
          throw std::runtime_error("Could not convert object to edge map.");
        }

        wutils::pack_cells_along_edges(self, *is_constrained_edge_option,
                                       pack_property_name);
      },
      py::arg("edge_is_constrained").none(false),
      py::arg("pack_property_name").none(false));

  module.def(
      "make_mesh",
      [process_mesh_output](wutils::Coordinates_array<Point>& vertices,
                            wutils::Face_connectivity<Surface_mesh>& faces,
                            bool with_vertices_map,
                            bool with_faces_map) -> py::object {
        wutils::Extension_data<Surface_mesh> data;
        auto mesh = wutils::make_mesh<Surface_mesh>(
            vertices, py::make_tuple(faces), data);
        return process_mesh_output(mesh.release(), data, with_vertices_map,
                                   with_faces_map);
      },
      py::arg("vertices").none(false), py::arg("faces").none(false),
      py::kw_only(), py::arg("with_vertices_map") = false,
      py::arg("with_faces_map") = false);
  module.def(
      "make_mesh",
      [process_mesh_output](wutils::Coordinates_array<Point>& vertices,
                            py::list& faces, bool with_vertices_map,
                            bool with_faces_map) -> py::object {
        wutils::Extension_data<Surface_mesh> data;
        auto mesh = wutils::make_mesh<Surface_mesh>(vertices, faces, data);
        return process_mesh_output(mesh.release(), data, with_vertices_map,
                                   with_faces_map);
      },
      py::arg("vertices").none(false), py::arg("faces").none(false),
      py::kw_only(), py::arg("with_vertices_map") = false,
      py::arg("with_faces_map") = false);

  wutils::wrap_property_map<Vertex_index>(module, pyclass, "vertex");
  wutils::wrap_property_map<Halfedge_index>(module, pyclass, "halfedge");
  wutils::wrap_property_map<Edge_index>(module, pyclass, "edge");
  wutils::wrap_property_map<Face_index>(module, pyclass, "face");

  return pyclass;
}

}  // namespace pyCGAL
