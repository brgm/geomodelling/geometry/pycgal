#pragma once

#include <pyCGAL/wrap/BGL/detail/iterators.h>

#include "Surface_mesh.h"

namespace pyCGAL::Surface_mesh {

using iterators = pyCGAL::wrap::BGL::detail::iterators<Surface_mesh>;

}  //  namespace pyCGAL::Surface_mesh
