#pragma once

#include <pyCGAL/wrap/Surface_mesh/detail/Tree.h>

#include "Surface_mesh.h"

namespace pyCGAL::Surface_mesh {

using Tree = pyCGAL::wrap::Surface_mesh::detail::Tree<Surface_mesh>;

}  //  namespace pyCGAL::Surface_mesh
