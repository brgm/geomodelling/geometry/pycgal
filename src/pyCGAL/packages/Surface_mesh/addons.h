#pragma once

#include <pyCGAL/wrap/Surface_mesh/detail/addons.h>

#include "Surface_mesh.h"

namespace pyCGAL::Surface_mesh {

using addons = pyCGAL::wrap::Surface_mesh::detail::addons<Surface_mesh>;

}  //  namespace pyCGAL::Surface_mesh
