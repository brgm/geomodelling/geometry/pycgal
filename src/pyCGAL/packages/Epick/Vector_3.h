#pragma once

#include <CGAL/Vector_3.h>

#include "Kernel.h"

namespace pyCGAL::Epick {
using Vector_3 = CGAL::Vector_3<Kernel>;
}  // namespace pyCGAL::Epick
