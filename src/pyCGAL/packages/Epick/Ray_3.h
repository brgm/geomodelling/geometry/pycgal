#pragma once

#include <CGAL/Ray_3.h>

#include "Kernel.h"

namespace pyCGAL::Epick {
using Ray_3 = CGAL::Ray_3<Kernel>;
}  // namespace pyCGAL::Epick
