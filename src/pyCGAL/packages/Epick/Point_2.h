#pragma once

#include <CGAL/Point_2.h>

#include "Kernel.h"

namespace pyCGAL::Epick {
using Point_2 = CGAL::Point_2<Kernel>;
}  // namespace pyCGAL::Epick
