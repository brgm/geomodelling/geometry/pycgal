#pragma once

#include <CGAL/Line_3.h>

#include "Kernel.h"

namespace pyCGAL::Epick {
using Line_3 = CGAL::Line_3<Kernel>;
}  // namespace pyCGAL::Epick
