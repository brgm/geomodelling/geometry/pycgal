#pragma once

#include <vector>

#include "Point_2.h"

namespace pyCGAL::Epick {
using Vector_of_Point_2 = std::vector<Point_2>;
}  // namespace pyCGAL::Epick
