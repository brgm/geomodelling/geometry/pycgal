#pragma once

#include <CGAL/Segment_2.h>

#include "Kernel.h"

namespace pyCGAL::Epick {
using Segment_2 = CGAL::Segment_2<Kernel>;
}  // namespace pyCGAL::Epick
