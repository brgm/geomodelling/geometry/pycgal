#pragma once

#include <pyCGAL/wrap/LinearGeometryKernel/extensions/Polyline.h>

#include "Point_3.h"

namespace pyCGAL::Epick {
using Polyline_3 = pyCGAL::extensions::LinearGeometryKernel::Polyline<Point_3>;
}  // namespace pyCGAL::Epick
