#pragma once

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

namespace pyCGAL::Epick {
using Kernel = CGAL::Epick;
}  // namespace pyCGAL::Epick
