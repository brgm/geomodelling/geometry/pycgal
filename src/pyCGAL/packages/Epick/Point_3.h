#pragma once

#include <CGAL/Point_3.h>

#include "Kernel.h"

namespace pyCGAL::Epick {
using Point_3 = CGAL::Point_3<Kernel>;
}  // namespace pyCGAL::Epick
