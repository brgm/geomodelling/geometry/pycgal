#pragma once

#include <CGAL/Plane_3.h>

#include "Kernel.h"

namespace pyCGAL::Epick {
using Plane_3 = CGAL::Plane_3<Kernel>;
}  // namespace pyCGAL::Epick
