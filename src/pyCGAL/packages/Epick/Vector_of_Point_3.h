#pragma once

#include <vector>

#include "Point_3.h"

namespace pyCGAL::Epick {
using Vector_of_Point_3 = std::vector<Point_3>;
}  // namespace pyCGAL::Epick
