#pragma once

#include <CGAL/Vector_2.h>

#include "Kernel.h"

namespace pyCGAL::Epick {
using Vector_2 = CGAL::Vector_2<Kernel>;
}  // namespace pyCGAL::Epick
