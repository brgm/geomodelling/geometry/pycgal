#pragma once

#include <CGAL/Segment_3.h>

#include "Kernel.h"

namespace pyCGAL::Epick {
using Segment_3 = CGAL::Segment_3<Kernel>;
}  // namespace pyCGAL::Epick
