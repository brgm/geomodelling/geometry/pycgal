#pragma once

#include <vector>

namespace pyCGAL::core {
using Vector_of_size_t = std::vector<std::size_t>;
}  // namespace pyCGAL::core
