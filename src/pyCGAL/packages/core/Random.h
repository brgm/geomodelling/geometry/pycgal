#pragma once

#include <CGAL/Random.h>

namespace pyCGAL::core {
using Random = CGAL::Random;
}  // namespace pyCGAL::core
