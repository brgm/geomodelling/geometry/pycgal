#pragma once

#include <CGAL/Bbox_3.h>

namespace pyCGAL::core {
using Bbox_3 = CGAL::Bbox_3;
}  // namespace pyCGAL::core
