#pragma once

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

namespace pyCGAL {
namespace Mesh_2 {
using Kernel = CGAL::Epick;
}  // namespace Mesh_2
}  // namespace pyCGAL
