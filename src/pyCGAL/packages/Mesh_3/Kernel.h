#pragma once

#include "../Epick/Kernel.h"

namespace pyCGAL {

namespace Mesh_3 {

using Kernel = pyCGAL::Epick::Kernel;

}  // namespace Mesh_3

}  //  namespace pyCGAL
