#pragma once

#include "../Surface_mesh/Surface_mesh.h"

namespace pyCGAL::Polygon_mesh_processing {

using Polygon_mesh = pyCGAL::Surface_mesh::Surface_mesh;

}  // namespace pyCGAL::Polygon_mesh_processing
