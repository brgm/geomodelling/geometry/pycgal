#pragma once

#include "../Epick/Plane_3.h"

namespace pyCGAL::Polygon_mesh_processing {

using Plane = pyCGAL::Epick::Plane_3;

}  // namespace pyCGAL::Polygon_mesh_processing
