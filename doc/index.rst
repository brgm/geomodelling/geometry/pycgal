================
pyCGAL |version|
================

You probably will have to refer to the
`CGAL documentation <https://doc.cgal.org/latest/Manual/index.html>`_
for most of the wrapped functions.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   python_reference/pycgal
   python_reference/modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

This documentation was built against pyCGAL version |release|.
